# Malquarium

> This repository is the "main hub" for the Malquarium
> project.
> It hosts the project wiki, contains all the components
> as git submodules and defines the overall release versions.

* [Wiki](https://codeberg.org/malquarium/malquarium/wiki)

